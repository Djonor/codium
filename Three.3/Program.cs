﻿using System;
using System.Text;

namespace Three._3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Give a number");
            var stars = int.Parse(Console.ReadLine());
            var sb = new StringBuilder();
            Console.WriteLine("n=" + stars);

            //first star
            for (var j = 0; j < stars; j++) sb.Append(" ");
            sb.Append("*");
            Console.WriteLine(sb);
            sb.Clear();

            //rest of stars
            for (var i = 1; i < stars; i++)
            {
                for (var j = 0; j < stars - i; j++) sb.Append(" ");
                sb.Append("*");
                for (var j = 1; j <= i; j++)
                    if (j == 1)
                        sb.Append(" ");
                    else
                        sb.Append("  ");
                sb.Append("*");

                Console.WriteLine(sb);
                sb.Clear();
            }
        }
    }
}