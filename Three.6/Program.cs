﻿using System;
using System.Text;

namespace Three._6
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Give a number");
            var number = int.Parse(Console.ReadLine());
            var counter = number * 2 - 2;
            var sb = new StringBuilder();
            Console.WriteLine("n=" + number);

            for (var i = 0; i < number * 2 - 1; i++)
            {
                if (i <= number - 1 || i >= number - 1)
                {
                    var x = i < number - 1 ? i : counter;

                    sb.Append("+");
                    for (var j = 0; j < number - 1 - x; j++) sb.Insert(0, "A");

                    if (x != 0)
                    {
                        for (var j = 0; j < x * 2 - 1; j++) sb.Append("E");
                        sb.Append("+");
                    }

                    for (var j = 0; j < number - 1 - x; j++) sb.Append("B");

                    if (i > number / 2 + 1)
                    {
                        sb.Replace("A", "C");
                        sb.Replace("B", "D");
                    }
                }

                Console.WriteLine(sb);
                sb.Clear();
                counter -= 1;
            }
        }
    }
}