﻿using System;
using System.Text;

namespace PrimeNumbers
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Give a number");
            var number = int.Parse(Console.ReadLine());
            var sb = new StringBuilder();

            sb.Append(number + " -> ");

            for (var i = 1; i <= number; i++)
            {
                var counter = 0;

                for (var j = 1; j <= i; j++)
                    if (i % j == 0)
                        counter++;
                if (counter == 2) sb.Append(i + " ");
            }

            Console.WriteLine(sb);
        }
    }
}