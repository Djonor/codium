﻿using System;
using System.Text;

namespace Three._2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Give a number");
            var stars = int.Parse(Console.ReadLine());
            var sb = new StringBuilder();
            Console.WriteLine("n=" + stars);
            for (var x = 1; x <= stars; x++)
            {
                for (var j = stars - x; j > 0; j--) sb.Append(" ");
                for (var i = 1; i <= x; i++) sb.Append("*");
                Console.WriteLine(sb);
                sb.Clear();
            }
        }
    }
}