﻿using System;
using System.Text;

namespace Three._5
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Give a number");
            var stars = int.Parse(Console.ReadLine());
            var counter = 1;
            var counter2 = 0;
            var sb = new StringBuilder();
            Console.WriteLine("n=" + stars);


            for (var j = 0; j < stars; j++)
            {
                //even # stars
                if (stars % 2 == 0)
                {
                    //middle
                    if (j == stars / 2 || j == stars / 2 - 1)
                    {
                        for (var i = 0; i < stars - 1; i++) sb.Append("*");
                    }
                    //stars under middle
                    else if (j < stars / 2)
                    {
                        //leading spaces
                        for (var i = 0; i < stars / 2 - j - 1; i++) sb.Append(" ");

                        for (var i = 0; i < j * 2 + 1; i++) sb.Append("*");
                    }
                    //stars after middle 
                    else if (j > stars / 2)
                    {
                        //leading spaces
                        for (var i = 0; i < 1 + 1 * counter2; i++) sb.Append(" ");

                        for (var i = 0; i < stars - (3 + counter2 * 2); i++) sb.Append("*");

                        counter2++;
                    }
                }

                //between first and middle row (uneven)
                else if (j < stars / 2)
                {
                    //leading spaces
                    for (var i = 0; i < stars / 2 - j; i++) sb.Append(" ");

                    for (var i = 0; i < j * 2 + 1; i++) sb.Append("*");
                }

                //middle (uneven)
                else if (j == stars / 2 && stars % 2 != 0)
                {
                    for (var i = 0; i < stars; i++) sb.Append("*");
                }

                //between middle and last line
                else if (j > stars / 2)
                {
                    //leading spaces
                    for (var i = 0; i < counter; i++) sb.Append(" ");

                    for (var i = 0; i < stars - counter * 2; i++) sb.Append("*");
                    counter = counter + 1;
                }

                Console.WriteLine(sb);
                sb.Clear();
            }
        }
    }
}