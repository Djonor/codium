﻿using System;
using System.Text;

namespace Three._4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Give a number");
            var stars = int.Parse(Console.ReadLine());
            var counter = 0;
            var sb = new StringBuilder();
            Console.WriteLine("n=" + stars);


            for (var i = 0; i < stars; i++)
            {
                //middle star (uneven)
                if (i == stars / 2 && stars % 2 != 0)
                {
                    for (var j = 0; j < i; j++) sb.Append(" ");

                    sb.Append("*");
                }
                //between first and middle row
                else if (i < stars / 2)
                {
                    for (var j = 0; j < i; j++) sb.Append(" ");

                    sb.Append("*");

                    for (var j = 0; j <= stars - 2 - (2 * i + 1); j++) sb.Append(" ");

                    sb.Append("*");
                }
                //between middle and final row (uneven stars)
                else if (i > stars / 2 && stars % 2 != 0)
                {
                    //leading spaces
                    for (var j = 0; j < stars - i - 1; j++) sb.Append(" ");

                    sb.Append("*");
                    //spaces between stars
                    for (var j = 0; j < 1 + counter * 2; j++) sb.Append(" ");

                    counter = counter + 1;
                    sb.Append("*");
                }

                //between middle and final row (even stars)
                else if (i >= stars / 2 && stars % 2 == 0)
                {
                    //leading spaces
                    for (var j = 0; j < stars / 2 - (counter + 1); j++) sb.Append(" ");

                    sb.Append("*");

                    //spaces between stars
                    for (var j = 0; j < counter * 2; j++) sb.Append(" ");

                    counter = counter + 1;
                    sb.Append("*");
                }

                Console.WriteLine(sb);
                sb.Clear();
            }
        }
    }
}