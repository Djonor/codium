﻿using System;
using System.Text;

namespace LeapYear
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var input = 1;
            var sb = new StringBuilder();
            sb.Append("Give a year: \n");
            Console.WriteLine(sb);
            while (input != 0)
            {
                input = int.Parse(Console.ReadLine());
                if (input % 400 == 0)
                    Print(input, true);
                else if (input % 100 != 0 && input % 4 == 0)
                    Print(input, true);
                else
                    Print(input, false);

                void Print(int year, bool boolean)
                {
                    Console.Clear();
                    if (boolean) sb.Append(year + " -> true\n");
                    else sb.Append(year + " -> false\n");
                    Console.WriteLine(sb);
                }
            }
        }
    }
}