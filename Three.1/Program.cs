﻿using System;

namespace Three._1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Give a number");
            var stars = int.Parse(Console.ReadLine());
            Console.WriteLine("n=" + stars);
            for (var i = 1; i <= stars; i++)
            {
                for (var j = 0; j < i; j++) Console.Write("*");
                Console.WriteLine("");
            }
        }
    }
}