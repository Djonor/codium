﻿using System;
using StackExchange.Redis;

namespace ToDoRedis
{
    internal class Program
    {
        //change ip:port to working redis server
        private static readonly ConnectionMultiplexer Muxer =
            ConnectionMultiplexer.Connect("34.77.115.255:6379,allowAdmin=true");

        private static readonly IDatabase conn = Muxer.GetDatabase();

        private static void Main(string[] args)
        {
            var listKey = "listKey";

            Console.WriteLine("Please make a choice" + "\n" + "1) See to do list" + "\n" + "2) Make new todo item" +
                              "\n" + "3) Delete item from list");

            var caseSwitch = Convert.ToInt32(Console.ReadLine());

            switch (caseSwitch)
            {
                case 1:
                    SeeList();
                    break;
                case 2:
                    MakeToDo();
                    break;
                case 3:
                    DeleteItem();
                    break;
                default:
                    Console.WriteLine("please give 1,2 or 3 as input");
                    break;
            }

            void SeeList()
            {
                for (var i = 0; i < conn.ListLength(listKey); i++)
                    Console.WriteLine(i + 1 + ") " + conn.ListGetByIndex(listKey, i));
            }

            void MakeToDo()
            {
                Console.WriteLine("Give a task for you ToDo list, to exit leave empty and press enter");
                while (true)
                {
                    var task = Console.ReadLine();
                    if (task == "") break;

                    conn.ListRightPush(listKey, task);
                }
            }

            void DeleteItem()
            {
                Console.WriteLine("Which item would you like to delete?");
                SeeList();
                var toDelete = Convert.ToInt32(Console.ReadLine());
                conn.ListRemove(listKey, conn.ListGetByIndex(listKey, toDelete - 1));
                SeeList();
            }
        }
    }
}